
Partial Class _Default
    Inherits System.Web.UI.Page



    Private Function LanzarPeticion(ByVal strPeticion As String) As Byte() ' System.Xml.XmlDocument
        'Realizamos la petici�n HTTP al servidor WMS de Secciones Censale

		Dim mensajeExc as String = ""
		
        Try

            Dim loHttp As Net.WebRequest = Net.WebRequest.Create(strPeticion)

            loHttp.Timeout = 60000

            Dim loWebResponse As Net.WebResponse
            Dim intentos As Integer = 0
            While intentos < 5
                intentos += 1
                Try
                    loWebResponse = loHttp.GetResponse()
                    Exit While
                Catch ex As Exception
                    If intentos >= 5 Then
                        Throw New Exception("Bucle intentos", ex)
                    End If
                End Try
            End While

            'Dim enc As Encoding = Encoding.GetEncoding(1252)

            'Dim theStream As IO.Stream = loWebResponse.GetResponseStream()
            'loWebResponse.Close()

            ''Dim buffer As Byte()
            'Dim reader As New IO.BufferedStream(theStream)

            'Dim res As Byte() '= readerReadBytes(theStream.Length)

            ''theStream.Read(buffer, 0, theStream.Length)
            'theStream.Close()


            'Return res

            'Dim enc As Encoding = Encoding.UTF8


            Context.Response.ContentType = loWebResponse.ContentType

            Dim theStream As IO.Stream = loWebResponse.GetResponseStream()
            Dim bytereader As New IO.BinaryReader(theStream)
            Dim buffer As Byte()
			
			

            If loWebResponse.ContentLength = -1 Then

                Dim memStream As New IO.MemoryStream()
                Dim currByte As Integer = bytereader.ReadByte()

                While currByte <> -1
                    memStream.WriteByte(currByte)
                    Try
                        currByte = bytereader.ReadByte()
                    Catch ex As IO.EndOfStreamException
                        'agm: Chapuza infame
                        currByte = -1
                    End Try
                End While

                buffer = memStream.ToArray()
                memStream.Close()
            Else
				mensajeExc = "llegamos antes de readbytes"
                buffer = bytereader.ReadBytes(loWebResponse.ContentLength)
				mensajeExc = "llegamos despues de readbytes"
            End If

            bytereader.Close()
            loWebResponse.Close()

            Return buffer

            'Dim loResponseStream As IO.StreamReader = New IO.StreamReader(loWebResponse.GetResponseStream(), enc)

            'Dim lcHtml As String = loResponseStream.ReadToEnd()
            'Dim docXml As System.Xml.XmlDocument

            'Dim i As Integer

            'docXml = New System.Xml.XmlDocument()
            'docXml.LoadXml(lcHtml)


            'loWebResponse.Close()

            'loResponseStream.Close()


            'Return docXml
        Catch ex As Exception
			Throw New Exception(mensajeExc & " " & ex.message, ex)
            'Return Nothing
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim params As NameValueCollection
        Dim strPeticion As String
        Dim strRedireccion As String
        Dim req As System.Collections.Specialized.NameValueCollection
        Dim strReq As String
        Try
            req = Request.QueryString
            strPeticion = req.Get("REQUEST")
            strReq = Request.Url.Query

            Select Case strPeticion.ToUpper()
                Case "GETCAPABILITIES"
                    Dim strVersion As String
                    If (strReq.ToUpper().Contains("VERSION=")) Then
                        Dim ind1 As Integer, ind2 As Integer


                        ind1 = strReq.ToUpper().IndexOf("VERSION=")
                        ind2 = strReq.ToUpper().IndexOf("&", ind1)

                        If (ind2 = -1) Then

                            ind2 = strReq.Length
                        End If
                        strVersion = strReq.Substring(ind1 + 8, ind2 - (ind1 + 8))

                    Else
                        strVersion = "1.1.0"
                    End If



                    Dim m_xmld As System.Xml.XmlDocument
                    Dim pathFichero As String, pathBase As String
                    Dim nombreFichero As String
                    nombreFichero = strVersion.Replace(".", "")

                    m_xmld = New System.Xml.XmlDocument()

                    'pathBase = System.AppDomain.CurrentDomain.BaseDirectory
                    pathBase = Page.MapPath(AppRelativeTemplateSourceDirectory)

                    pathFichero = System.Configuration.ConfigurationManager.AppSettings.Get("PathCapabilities")
                    Dim sr As System.IO.StreamReader
                    sr = New System.IO.StreamReader(pathBase + pathFichero + nombreFichero + ".xml")
                    Dim strCapabilities As String
                    strCapabilities = sr.ReadToEnd()
                    sr.Close()

                    Response.Clear()
                    Response.ContentType = "text/xml"
                    Response.Write(strCapabilities)
                    Exit Select
                Case Else
                    'params = Request.QueryString 'Obtenemos la colecci�n de par�metros
                    strRedireccion = System.Configuration.ConfigurationManager.AppSettings("ServicioAGS")
                    strPeticion = strRedireccion + Request.Url.Query

                    'strPeticion = IIf(LTrim(RTrim(params.GetValues("peticion")(0))).Equals(Nothing), "", LTrim(RTrim(params.GetValues("peticion")(0))))

                    'Dim theStream As IO.Stream = LanzarPeticion(strPeticion)

                    'Context.Response.ContentType = "text/xml"
                    'Context.Response.Charset = "utf-8"

                    Context.Response.Clear()

                    Response.BinaryWrite(LanzarPeticion(strPeticion))

                    Exit Select


            End Select



            Return




        Catch ex As Exception
            Context.Response.ContentType = "text/string"
            Context.Response.Charset = "utf-8"
            Context.Response.Write(ex.Message)
            Context.Response.End()
        End Try
        Context.Response.End()

    End Sub
End Class
