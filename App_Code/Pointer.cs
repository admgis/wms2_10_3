namespace GotDotNet.XPointer {
    using System;
    using System.Xml;
    
	/// <summary>
	/// Abstract XPointer pointer.
	/// </summary>
	public abstract class Pointer {	
	    /// <summary>
	    /// Evaluates XPointer pointer and returns pointed nodes.
	    /// </summary>
	    /// <param name="doc">Document to evaluate XPointer on.</param>
	    /// <returns></returns>	    					
		public abstract XmlNodeList Evaluate(XmlDocument doc);
	}
}
