namespace GotDotNet.XPointer {
    using System;
    using System.Xml;

	/// <summary>
	/// Shorthand XPointer pointer.
	/// </summary>
	internal class ShorthandPointer : Pointer {
	    string _NCName;	    
	    	    	    
	    public string NCName {
	        get { return _NCName; }
	    } 
	    
		public ShorthandPointer(string n) {
            _NCName = n;					
		}
		
        public override XmlNodeList Evaluate(XmlDocument doc) {            
            XmlNodeList result = doc.SelectNodes("id('"+ _NCName + "')");
            if (result != null && result.Count > 0)
                return result;
            else                
                throw new NotFoundException("XPointer doesn't identify any subresource");                
        }				
	}
}
