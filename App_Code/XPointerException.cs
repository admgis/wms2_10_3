namespace GotDotNet.XPointer {
    using System;
    
	/// <summary>
	/// Generic XPointer exception.
	/// </summary>
	public abstract class XPointerException : ApplicationException {
        public XPointerException(string message) : base(message) {}
        public XPointerException(string message, Exception innerException)
            : base(message, innerException) {}
	}
	
    public class XPointerSyntaxException : XPointerException {
        public XPointerSyntaxException(string message) : base(message) {}
        public XPointerSyntaxException(string message, Exception innerException)
            : base(message, innerException) {}
    }
            
    public class NotFoundException : XPointerException {
        public NotFoundException(string message) : base(message) {}
        public NotFoundException(string message, Exception innerException)
            : base(message, innerException) {}
    }
}
