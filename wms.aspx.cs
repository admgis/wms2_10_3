﻿using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Collections;
using System.IO;
using GotDotNet.XInclude;
using System.Xml;
using System.Linq;

public partial class _Default : System.Web.UI.Page
{
    private const string _KEY_LAYERSREST = "KEYLAYERSREST_";
    private static string[] separadorGrupos = new string[] { "|" };
    private static string[] separadorCapas = new string[] { "," };

    private double escalaMin = 16000000;
    private SOE_Tematico.DynamicService_DynamicServiceMAGRAMA wsArcGISServiceExtension = null;
    private SOE_Tematico.GetWMSDataResponse respAGSExtension = null;
    private const string LayerDesc_WSAGSExtension = "";//mantenemos este parámetro en blanco, pues en WMS no vamos a poder utilizar servicios dinámicos

    private string RUTA_TRAZAS = System.Configuration.ConfigurationManager.AppSettings["RutaTrazas"];
    private string pathFicheroLog = System.Configuration.ConfigurationManager.AppSettings["pathLog"];
    private string ServidorArcGIS = System.Configuration.ConfigurationManager.AppSettings.Get("ServidorArcGIS");
    private string ServicioArcGIS = System.Configuration.ConfigurationManager.AppSettings.Get("ServicioArcGIS");
    private string trazaGeneral = "";

    private NameValueCollection RequestQueryString;
    private string strQueryString;

    #region LayersREST

    public static System.Xml.XmlNodeList getLayersRest(string nombreServidor, string nombreServicio)
    {
        System.Xml.XmlNodeList nodosLayer = HttpContext.Current.Application[_KEY_LAYERSREST + nombreServicio] as System.Xml.XmlNodeList;

        if (nodosLayer == null)
        {
            String urlLayers = "http://" + nombreServidor + "/ArcGIS/rest/services/" + nombreServicio + "/MapServer/layers?f=json";
            System.Xml.XmlDocument xmlLayersREST = getXMLFromJsonUrl(urlLayers);
            nodosLayer = xmlLayersREST.GetElementsByTagName("layers");

            HttpContext.Current.Application[_KEY_LAYERSREST + nombreServidor + "_" + nombreServicio] = nodosLayer;
        }

        return nodosLayer;
    }

    public static System.Xml.XmlDocument getXMLFromJsonUrl(string url)
    {
        byte[] buffer = getBytesUrl(url);

        System.Xml.XmlReader reader =
            System.Runtime.Serialization.Json.JsonReaderWriterFactory
                .CreateJsonReader(buffer, new System.Xml.XmlDictionaryReaderQuotas());

        System.Xml.XmlDocument res = new System.Xml.XmlDocument();
        res.Load(reader);

        return res;
    }

    public static byte[] getBytesUrl(string url)
    {
        try
        {
            System.Net.HttpWebRequest req = System.Net.HttpWebRequest.Create(url) as System.Net.HttpWebRequest;
            System.Net.HttpWebResponse resp = req.GetResponse() as System.Net.HttpWebResponse;
            System.IO.Stream str = resp.GetResponseStream();
            System.IO.StreamReader read = new System.IO.StreamReader(str, System.Text.Encoding.Default);

            string datos = read.ReadToEnd();
            byte[] bytesImagen = System.Text.Encoding.Default.GetBytes(datos);
            resp.Close();
            read.Close();
            return bytesImagen;
            

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region Google Analytics

    private string getIP(HttpContext c)
    {
        string ips = c.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ips))
        {
            return ips.Split(',')[0];
        }
        return c.Request.ServerVariables["REMOTE_ADDR"];
    }
    private string getCID()
    {
        string cid = Int32.Parse((new Random()).Next().ToString()) +
                    "." + (DateTime.Now.Millisecond);

        return cid;
    }
    // arg 2016-11-03: eventos de estadística - los gestionamos a través de un diccionario interno
    // http://www.carballude.es/blog/2012/01/21/c-los-peligros-de-las-variables-estaticas-en-librerias/comment-page-1/
    // static readonly porque actúa como una constante que queremos que al compilar se actualice
    private static readonly Dictionary<string, string> statsEventsDict = new Dictionary<string, string>()
	{
        // operaciones SLD
        {"GETLEGENDGRAPHIC", "Pedir Leyenda"}, 
        {"GETSTYLES", "Pedir Estilos"},
        // Versión 1.0.0
        {"MAP", "Cambiar Extensión"}, 
        {"FEATURE_INFO", "Identificar"},
        // Versiones posteriores
	    {"GETMAP", "Cambiar Extensión"},
        {"GETFEATUREINFO", "Identificar"}
	};
    // arg 2017-05-29 se solicitó que el getsld estuviera fuera de las peticiones
    // registradas por estadísticas.
    /// <summary>
    /// Lista de operaciones WMS excluídas del registro de estadísticas.
    /// </summary>
    private static readonly string[] statsExceptionsArray = new string[]{
        "CAPABILITIES", "GETCAPABILITIES", "GETSLD"
    };
    private void sendGoogleAnalytics(string pageName)
    {
        var postData = "v=1";              // Version.
        postData += "&tid=" + System.Configuration.ConfigurationManager.AppSettings["TrackingID"];  // Tracking ID / Property ID.

        if (Request.Cookies != null)
        {
            if (Request.Cookies["_ga"] != null)
            {
                string cookieGA = Request.Cookies["_ga"].Value;
                // Trazar("cookieGA: " + cookieGA.ToString());
                if (cookieGA != null)
                {
                    string[] splitCookie = cookieGA.Split(new char[] { '.' });
                    // 2016-11-07 arg: daba un fallo si generábamos nosotros el CID porque se recuperaba un CID que, a lo sumo,
                    // tenía 2 elementos en el array e impedía el registro de estadísticas en algunos clientes
                    if (splitCookie.Length < 4)
                    {
                        postData += "&cid=" + cookieGA.ToString();     // recuperamos el cookieGA directamente
                    }
                    else
                    {
                        postData += "&cid=" + splitCookie[2] + "." + splitCookie[3];         // Anonymous Client ID.
                    }
                }
            }
            else
            {
                string cid = getCID();         // Anonymous Client ID.
                HttpCookie cook = new HttpCookie("_ga", cid);
                //HttpCookie test = new HttpCookie("_test", cid);
                Response.Cookies.Add(cook);
                //Response.Cookies.Add(test);
                postData += "&cid=" + cid;
            }
        }
        else
        {
            string cid = getCID();         // Anonymous Client ID.
            HttpCookie cook = new HttpCookie("_ga", cid);
            //HttpCookie test = new HttpCookie("_test", cid);
            //Response.Cookies.Add(test);
            Response.Cookies.Add(cook);
            postData += "&cid=" + cid;
        }

        // identificación del evento o página vista
        string eventValue = null;
        string operation = Request.QueryString["REQUEST"]; // da igual mayúsculas que minúsculas en la URL en ASP.NET
        if (operation != null)
        {
            statsEventsDict.TryGetValue(operation.ToUpper(), out eventValue);
        }
        if (eventValue != null)
        {
            postData += "&t=event";     // Event hit type.
            postData += "&ec=WMS";
            postData += "&ea=" + eventValue;
        }
        else
        {
            postData += "&t=pageview";     // Pageview hit type.
        }

        postData += "&uip=" + getIP(HttpContext.Current);     // IP address override.
        postData += Request.ServerVariables["HTTP_USER_AGENT"];
        postData += "&dp=" + HttpUtility.UrlEncode(pageName);
        postData += "&dh=" + Request.Url.Host;

        // arg: dejamos un log para controlar las peticiones
        // Trazar(postData.ToString());

        var data = Encoding.ASCII.GetBytes(postData);

        var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://www.google-analytics.com/collect?" + postData);

        System.Net.WebProxy myproxy = new System.Net.WebProxy(ConfigurationManager.AppSettings.Get("proxyUrl"), int.Parse(ConfigurationManager.AppSettings.Get("proxyPort")));
        myproxy.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("proxyUser"), ConfigurationManager.AppSettings.Get("proxyPwd"));
        myproxy.BypassProxyOnLocal = false;
        request.Proxy = myproxy;


        request.Method = "GET";/*
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = data.Length;*/

        /*
        using (var stream = request.GetRequestStream())
        {
            stream.Write(data, 0, data.Length);
        }*/

        var response = (System.Net.HttpWebResponse)request.GetResponse();
    }
    
    private bool registrarPeticionParaEstadisticas()
    {
        var exceps = statsExceptionsArray;
        string reqStr = Request.QueryString["REQUEST"];
        if (String.IsNullOrEmpty(reqStr))
            return false;
        reqStr = reqStr.ToUpper();
        if (System.Array.IndexOf(exceps, reqStr) == -1)
        {
            try
            {
                sendGoogleAnalytics(Request.Url.AbsolutePath);
                return true;
            }
            catch
            {
            }
        }
        return false;
    }

    #endregion

    #region Funciones gestión layers

    // 20180111 @ADR: Estas funciones cotejan y convierten las capas solicitadas por el cliente con las correspondientes entendibles por el ArcGIS for Server.

    private string[] ObtenerCapasMXD(string[] arrLayersQueryString, string[] capasMXDConfig, string[] capasConfig)
    {
        string[] capasMXD = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            capasMXD[i] = capasMXDConfig.ElementAt(indiceConfig);
        }
        return capasMXD;
    }

    private string[] ObtenerCapasIdentificacion(string[] arrLayersQueryString, string[] capasIdentificacionConfig, string[] capasConfig)
    {
        string[] capasIdentificacion = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            capasIdentificacion[i] = capasIdentificacionConfig.ElementAt(indiceConfig);
        }
        return capasIdentificacion;
    }

    private string[] ObtenerTitulosMXD(string[] arrLayersQueryString, string[] titulosMXDConfig, string[] capasConfig, string[] capasMXDConfig)
    {
        string[] titulosMXD = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            if (titulosMXDConfig.Length > 0)
            {
                titulosMXD[i] = titulosMXDConfig.ElementAt(indiceConfig);
            }
            else
            {
                titulosMXD[i] = capasMXDConfig.ElementAt(indiceConfig);
            }
        }
        return titulosMXD;
    }
    private string[] ObtenerTituloGrupo(string[] arrLayersQueryString, string[] titulosConfig, string[] capasConfig)
    {
        string[] tituloGrupo = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            tituloGrupo[i] = titulosConfig.ElementAt(indiceConfig);
        }
        return tituloGrupo;
    }

    private string[] ObtenerCapasPeticion(string[] capasMXD, string[] capasIdentificacion, string[] titulosMXD)
    {
        string[] capasMXDfil = new string[capasMXD.Length];
        for (int i = 0; i < capasMXD.Length; i++)
        {
            string[] capasMXDInd = capasMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            string[] capasIdentificacionInd = capasIdentificacion[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosMXDInd = titulosMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            for (int j = 0; j < capasMXDInd.Length; j++)
            {
                if (capasIdentificacionInd[j] == "0")
                {
                    capasMXDInd[j] = "";
                    titulosMXDInd[j] = "";
                }
            }
            capasMXDfil[i] = string.Join(",", capasMXDInd);
            titulosMXD[i] = string.Join(",", titulosMXDInd);
            string[] capasMXDIndNew = capasMXDfil[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosMXDIndNew = titulosMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            capasMXDfil[i] = string.Join(",", capasMXDIndNew);
            titulosMXD[i] = string.Join(",", titulosMXDIndNew);
        }
        ArrayList capasPeticionSalida = new ArrayList(capasMXDfil);
        if (capasPeticionSalida.IndexOf("") != -1)
        {
            capasPeticionSalida.RemoveAt(capasPeticionSalida.IndexOf(""));
        }
        String[] capasPeticion = (String[])capasPeticionSalida.ToArray(typeof(string));
        return capasPeticion;
    }
    
    private double calcularEscala(string SRS, double minX, double minY, double maxX, double maxY, int ancho, int alto)
    {
        DateTime horaInicio = DateTime.Now;
        StringBuilder strBuilder = new StringBuilder();

        int sistemaRef;
        sistemaRef = int.Parse(SRS.Split(":".ToCharArray())[1]);

        // Si es CRS:84 se convierte a EPSG:4326
        if (sistemaRef == 84)
            sistemaRef = 4326;

        double escala = wsArcGISServiceExtension.ComputeScale(sistemaRef, minX, maxX, minY, maxY, ancho, alto);

        TimeSpan diff = DateTime.Now.Subtract(horaInicio);
        strBuilder.Append("Escala calculada: 1:" + Math.Round(escala, 0) + ". ");
        strBuilder.Append("Escala pedida en " + sistemaRef.ToString() + ". ");
        strBuilder.Append(diff.TotalMilliseconds.ToString() + " ms");

        // 20140613 @ALX: Habilita la traza por web.config
        bool habilitarTraza = false;
        if (System.Configuration.ConfigurationManager.AppSettings["habilitarTraza"] != null)
            habilitarTraza = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["PermitirSTYLES"]);
        if (habilitarTraza)
            Trazar(strBuilder.ToString()); //@ALX: genera un log para el cálculo de escalas (path: RutaTrazas en WebConfig)

        return escala;

    }

    #endregion

    #region Utilidades

    private static NameValueCollection ConstruirQueryString(string peticionURL)
    {
        NameValueCollection queryString = new NameValueCollection();

        // Elimina el ? del principio
        if (peticionURL.IndexOf("?") == 0)
            peticionURL = peticionURL.Remove(0, 1);

        // Se sustraen los parámetros como nombre/valor
        string[] pairs = peticionURL.Split("&".ToCharArray());
        foreach (string par in pairs)
        {
            // Se separan en dos
            string nombre = "", valor = "";
            var equalIndex = par.IndexOf("=");
            if (equalIndex > -1)
            {
                nombre = par.Substring(0, equalIndex);
            }
            if (!String.IsNullOrEmpty(nombre))
            {
                valor = par.Substring(equalIndex + 1);
                queryString.Add(nombre, valor);
            }
        }

        return queryString;
    }

    private string reconstruirStrQueryString(NameValueCollection queryString)
    {
        Array copyNames = Array.CreateInstance(typeof(string), queryString.Count);
        Array copyValues = Array.CreateInstance(typeof(string), queryString.Count);
        queryString.AllKeys.CopyTo(copyNames, 0);
        queryString.CopyTo(copyValues, 0);
        string strQueryString = "";
        string strSep = "";
        for (int iReq = 0; iReq < copyNames.Length; iReq++)
        {
            strQueryString += strSep + copyNames.GetValue(iReq).ToString() + "=" + copyValues.GetValue(iReq).ToString();
            strSep = "&";
        }
        return strQueryString;
    }

    private static string ExtraerQueryURL(string peticionURL)
    {
        // Se elimina la parte invariable de la petición y solo se devuelven los parámetros
        int indice1 = peticionURL.IndexOf("?");
        peticionURL = (indice1 != -1) ? (indice1 < peticionURL.Length - 1) ? peticionURL.Substring(indice1 + 1) : null : null;

        return peticionURL;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="mensaje"></param>
    public void Trazar(string mensaje)
    {
        //return;
        System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(RUTA_TRAZAS);
        if (dirInfo.Exists == false)
        {
            dirInfo.Create();
        }
        DateTime fecha = DateTime.Now;

        System.IO.FileStream fs = new System.IO.FileStream(RUTA_TRAZAS + "\\Traza" +
                        fecha.Year.ToString() + fecha.Month.ToString() + fecha.Day.ToString() + ".log",
                        FileMode.Append, FileAccess.Write,
                        FileShare.Write);

        System.IO.StreamWriter sw = null;
        try
        {
            sw = new StreamWriter(fs);
            string strMSeconds = "00" + fecha.Millisecond.ToString();
            strMSeconds = strMSeconds.Substring(strMSeconds.Length - 3, 3);

            sw.WriteLine("[" + fecha.ToLongTimeString() + "." + strMSeconds + "]" + mensaje);
            sw.Flush();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            sw.Close();
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string myPath = this.Page.MapPath(this.AppRelativeTemplateSourceDirectory);
            bool estadisticasRegistradas = registrarPeticionParaEstadisticas();
            string strREQUEST = Request.QueryString["REQUEST"];
            if (strREQUEST == null) strREQUEST = "";

            string tolerancia = System.Configuration.ConfigurationManager.AppSettings["Tolerancia"];
            string idServicio = System.Configuration.ConfigurationManager.AppSettings.Get("IdServicio");

            byte[] bytes_WMSResponse = null;
            string responseContentType = "";
            string httpPostData = "";
            string strQueryStringSLDTematico = "";
            string nombreCapaSTYLESTematico = "";
            bool cambio_FormatInfo_FeatureInfo = false;

            string layerDefs = Request["layerDefs"];
            if (layerDefs == null)
                layerDefs = "";
            
            Encoding codificacion = Encoding.GetEncoding("UTF-8");

            // RequestQueryString = Request.QueryString; //no vale por el problema de codificación que tratamos aquí
            //
            // Problema de codificación
            // arg subsanación infame, pero funciona con todos los gis de escritorio probados y navegadores
            string strUrlDecodificada;
            if (Request.Url.Query.Contains("%EF%BF%BD")) // Caracter incapaz de codificar
                strUrlDecodificada = Request.RawUrl.ToString();
            else
                strUrlDecodificada = Request.Url.ToString();

            strUrlDecodificada = HttpUtility.UrlDecode(strUrlDecodificada, Encoding.UTF8); // para udig FeatureInfo
            // ---------------------------------------------------------------------------------------------

            strQueryString = ExtraerQueryURL(strUrlDecodificada);
            RequestQueryString = ConstruirQueryString(strQueryString);

            Encoding encEntrada = Encoding.GetEncoding("UTF-8");
            string SLDstrEstilos = "";
            string SLDstrGrupos = "";
            string[] peticiones = null;
            string[] titulosMXD = null;
            byte[] respuesta = null;

            #region Lectura Config y Petición

            string strPeticion = RequestQueryString.Get("REQUEST");
            string stylesReq = RequestQueryString.Get("STYLES");
            string layersReq = RequestQueryString.Get("LAYERS");
            string layerReq = RequestQueryString.Get("LAYER");
            string queryLayersReq = RequestQueryString.Get("QUERY_LAYERS");

            string[] capasConfig = System.Configuration.ConfigurationManager.AppSettings.Get("Capas").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] capasMXDConfig = System.Configuration.ConfigurationManager.AppSettings.Get("CapasMXD").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] capasIdentificacionConfig = System.Configuration.ConfigurationManager.AppSettings.Get("CapasIdentificables").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosConfig = System.Configuration.ConfigurationManager.AppSettings.Get("Titulos").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosMXDConfig = System.Configuration.ConfigurationManager.AppSettings.Get("TitulosMXD").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);

            string[] capasMXD = null;
            string[] capasIdentificacion = null;
            string[] capasPeticiones = null;

            string[] arrQueryLayersReq = null;
            string[] arrLayerReq = null;
            string[] arrLayersReq = null;

            if (layersReq != null)
            {
                arrLayersReq = layersReq.Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            }
            if (queryLayersReq != null)
            {
                arrQueryLayersReq = queryLayersReq.Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            }
            if (layerReq != null)
            {
                arrLayerReq = layerReq.Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            }

            #endregion

            //  1: Tratamiento general para varios tipos de peticiones

            if (strREQUEST.Equals("GETSLD", StringComparison.CurrentCultureIgnoreCase) || strREQUEST.Equals("GETSTYLES", StringComparison.CurrentCultureIgnoreCase))
            {
                string SLDpathEstilos = System.Configuration.ConfigurationManager.AppSettings.Get("PathEstilos");
                string SLDnombreFicheroEstilos = System.Configuration.ConfigurationManager.AppSettings.Get("NombreFicheroEstilos");
                string SLDgrupos = myPath + "/estilos/SLDgrupos.xml";
                XIncludingReader xir=null;
                XmlTextReader r =null;
                XIncludingReader xit = null;
                XmlTextReader t = null;
                try
                {
                    r = new XmlTextReader(SLDpathEstilos + SLDnombreFicheroEstilos);
                    xir = new XIncludingReader(r);
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xir);
                    SLDstrEstilos = doc.InnerXml.ToString();
                    xir.Close();
                    // 20171128 @ADR: En caso de existir el nuevo SLDgrupos.xml (ver #3304) se procede a su lectura
                    if (strPeticion.Equals("GETSTYLES", StringComparison.CurrentCultureIgnoreCase) && File.Exists(SLDgrupos))
                    {
                        t = new XmlTextReader(SLDgrupos);
                        xit = new XIncludingReader(t);
                        XmlDocument doct = new XmlDocument();
                        doct.Load(xit);
                        SLDstrGrupos = doct.InnerXml.ToString();
                        xit.Close();
                    }
                    else if (strPeticion.Equals("GETSLD", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Response.Clear();
                        Response.ContentType = responseContentType;
                        string SLDnombreFicheroSalida = "attachment; filename=SLD.xml";
                        Response.AddHeader("content-disposition", SLDnombreFicheroSalida);
                        Response.Write(SLDstrEstilos);
                    }
                }
                catch (Exception ex)
                {
                    string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + "  ]]></ServiceException></ServiceExceptionReport>";
                    Response.Clear();
                    Response.ContentType = "text/xml";
                    Response.Write(strError);
                    if (xir != null)
                        xir.Close();
                    return;
                }
            }

            if (strREQUEST.Equals("GETFEATUREINFO", StringComparison.CurrentCultureIgnoreCase))
            {
                if (tolerancia != null)
                {
                    int intTolerancia = int.Parse(tolerancia);

                    if (intTolerancia != 1)
                    {

                        NameValueCollection req2 = new NameValueCollection(RequestQueryString);
                        if (RequestQueryString.Get("WIDTH") != null)
                        {
                            decimal width = decimal.Parse(RequestQueryString.Get("WIDTH"));
                            req2.Set("WIDTH", decimal.Round(width / intTolerancia, 0).ToString());
                            
                        }
                        if (RequestQueryString.Get("HEIGHT") != null)
                        {
                            decimal height = decimal.Parse(RequestQueryString.Get("HEIGHT"));
                            req2.Set("HEIGHT", decimal.Round(height / intTolerancia, 0).ToString());
                        }
                        if (RequestQueryString.Get("I") != null)
                        {
                            decimal identI = decimal.Parse(RequestQueryString.Get("I"));
                            req2.Set("I", decimal.Round(identI / intTolerancia, 0).ToString());
                        }
                        if (RequestQueryString.Get("J") != null)
                        {
                            decimal identJ = decimal.Parse(RequestQueryString.Get("J"));
                            req2.Set("J", decimal.Round(identJ / intTolerancia, 0).ToString());
                        }
                        if (RequestQueryString.Get("X") != null)
                        {
                            decimal identX = decimal.Parse(RequestQueryString.Get("X"));
                            req2.Set("X", decimal.Round(identX / intTolerancia, 0).ToString());
                        }
                        if (RequestQueryString.Get("Y") != null)
                        {
                            decimal identY = decimal.Parse(RequestQueryString.Get("Y"));
                            req2.Set("Y", decimal.Round(identY / intTolerancia, 0).ToString());
                        }


                        RequestQueryString = req2;

                        strQueryString = reconstruirStrQueryString(RequestQueryString);
                    }
                }
            }
            else if (strREQUEST.Equals("GETSTYLES", StringComparison.CurrentCultureIgnoreCase))
            {
                NameValueCollection auxQS = new NameValueCollection(RequestQueryString);
                string qsParam = "LAYERS";

                if (strREQUEST.Equals("GetLegendGraphic", StringComparison.CurrentCultureIgnoreCase))
                {
                    qsParam = "LAYER";
                }

                if (RequestQueryString.Get(qsParam) != null)
                {
                    nombreCapaSTYLESTematico = RequestQueryString.Get(qsParam);
                    // 20180402 @ADR: Es necesario enviar el nombre de las capas cuyos estilos se quieren consultar
                    capasMXD = ObtenerCapasMXD(arrLayersReq, capasMXDConfig, capasConfig);
                    string strCapasMXD = string.Join(",", capasMXD);
                    auxQS.Set(qsParam, strCapasMXD);
                }

                qsParam = "QUERY_LAYERS";

                if (RequestQueryString.Get(qsParam) != null)
                {
                    nombreCapaSTYLESTematico = RequestQueryString.Get(qsParam);
                    auxQS.Set(qsParam, "0");
                }
                
                strQueryStringSLDTematico = reconstruirStrQueryString(auxQS);

            }

            //  2: Tratamiento de parámetros específicos de cada petición. GET y POST
            switch (Request.HttpMethod)
            {
                case "GET":
                    if (strQueryString.Length > 0)
                    {
                        if (strREQUEST.Equals("GETFEATUREINFO", StringComparison.CurrentCultureIgnoreCase))
                        {
                            // Si se pide un formato text/html se cambia

                            if (RequestQueryString.Get("INFO_FORMAT") != null &&
                                RequestQueryString.Get("INFO_FORMAT").ToLower() == "text/html")
                            {
                                RequestQueryString.Set("INFO_FORMAT", "application/vnd.esri.wms_featureinfo_xml");
                                strQueryString = reconstruirStrQueryString(RequestQueryString);
                                cambio_FormatInfo_FeatureInfo = true;
                            }
                        }

                        else if (strREQUEST.Equals("GETMAP", StringComparison.CurrentCultureIgnoreCase) ||
                            (strREQUEST.Equals("GETSTYLES", StringComparison.CurrentCultureIgnoreCase)))
                        {
                            capasMXD = ObtenerCapasMXD(arrLayersReq, capasMXDConfig, capasConfig);
                            if (stylesReq != "")
                            {
                                for (int k = 0; k < capasMXD.Length; k++)
                                {
                                    string[] capasMXDsplit = capasMXD[k].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                                    string[] stylesMXDsplit = new string[capasMXDsplit.Length];
                                    for (int j = 0; j < stylesMXDsplit.Length; j++)
                                    {
                                        stylesMXDsplit[j] = stylesReq;
                                    }
                                    string stylesNewReq = string.Join(",", stylesMXDsplit);
                                    RequestQueryString.Set("STYLES", stylesNewReq);
                                    strQueryString = reconstruirStrQueryString(RequestQueryString);
                                }
                            }
                            RequestQueryString.Set("LAYERS", capasMXD[0]);
                            strQueryString = reconstruirStrQueryString(RequestQueryString);
                        }

                        else if (strREQUEST.Equals("GETLEGENDGRAPHIC", StringComparison.CurrentCultureIgnoreCase))
                        {
                            for (int i = 0; i < capasMXDConfig.Length; i++)
                            {
                                if (capasMXDConfig[i].Contains(layerReq) && capasMXDConfig[i].Contains(","))
                                {
                                    strQueryString = HttpUtility.UrlEncode(strQueryString, System.Text.Encoding.UTF8).Replace("+", "%20");
                                    break;
                                }
                                else
                                {
                                    capasMXD = ObtenerCapasMXD(arrLayerReq, capasMXDConfig, capasConfig);
                                    for (int k = 0; k < capasMXD.Length; k++)
                                    {
                                        string[] capasMXDsplit = capasMXD[k].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                                        RequestQueryString.Set("LAYER", capasMXDsplit[0]);
                                        strQueryString = reconstruirStrQueryString(RequestQueryString);
                                    }
                                    strQueryString = HttpUtility.UrlEncode(strQueryString, System.Text.Encoding.UTF8).Replace("+", "%20");
                                }
                            }
                        }
                    }
                    break;

                // HTTP POST. No está hecho. No es necesario
                case "POST":

                    System.IO.StreamReader strReader;
                    strReader = new System.IO.StreamReader(Page.Request.InputStream, codificacion);
                    httpPostData = strReader.ReadToEnd();

                    // TODO arg comprobar esto
                    httpPostData = HttpUtility.UrlDecode(httpPostData, codificacion); // UTF-8
                    
                    //Encontramos la petición WMS (REQUEST)
                    if (httpPostData.ToUpper().Contains("GETMAP"))
                    {
                        //agm: 29/01/2013 levantamos esta restricción,NO permitimos estilos
                        strREQUEST = "GETMAP";
                        //if (!configPermitirEstilos)
                        //{
                            int indice = httpPostData.ToUpper().IndexOf("STYLES");
                            indice = httpPostData.ToUpper().IndexOf("=", indice);
                            int indice2 = httpPostData.ToUpper().IndexOf("&", indice + 1);
                            if (indice2 == -1)
                                indice2 = httpPostData.Length; // Hasta el final del petición
                            httpPostData = httpPostData.Substring(0, indice + 1) + httpPostData.Substring(indice2);
                        //}

                        //  TODO: Aplicar cambio de estilos 
                    }
                    else if (httpPostData.ToUpper().Contains("GETCAPABILITIES"))
                    {
                        strREQUEST = "GETCAPABILITIES";
                    }
                    else if (httpPostData.ToUpper().Contains("GETFEATUREINFO"))
                    {
                        strREQUEST = "GETFEATUREINFO";
                        
                        if (httpPostData.ToUpper().Contains("TEXT/HTML"))
                        {
                            //post = post.Replace ("text/html","text/xml");
                            //  post = post.Replace("text/html", "application/vnd.ogc.wms_xml");

                            httpPostData = httpPostData.Replace("text/html", "application/vnd.esri.wms_featureinfo_xml");
                            cambio_FormatInfo_FeatureInfo = true;
                        }
                    }

                    else
                    {
                        strREQUEST = "";
                    }

                    break;
            }

            wsArcGISServiceExtension = new SOE_Tematico.DynamicService_DynamicServiceMAGRAMA();
            wsArcGISServiceExtension.Url = wsArcGISServiceExtension.Url.Replace("/DynamicService/", "/"+ServicioArcGIS+"/");

            // 3.   Generación respuesta

            switch (strREQUEST.ToUpper())
            {
                case "GETSLD":
                    break;

                case "GETSTYLES":   // generamos un XML similar al que devuelve el servidor, a partir de nuestro SLD.xml

                    bool hayRespuestaCompuesta = false;
                    if (Request.HttpMethod == "GET")
                    {
                        for (int i = 0; i < capasMXD.Length; i++)
                        {
                            if (capasMXD[i].Contains(",") || layersReq.Contains(","))
                            {
                                hayRespuestaCompuesta = true; 
                            }
                        }

                        SOE_Tematico.DynamicService_DynamicServiceMAGRAMA wsArcGISServiceExt_Dynamic = new SOE_Tematico.DynamicService_DynamicServiceMAGRAMA();
                        strQueryStringSLDTematico = HttpUtility.UrlEncode(strQueryStringSLDTematico, System.Text.Encoding.UTF8).Replace("+", "%20");
                        httpPostData = wsArcGISServiceExtension.GetToPost(strQueryStringSLDTematico);
                        respAGSExtension = wsArcGISServiceExtension.get_Data(LayerDesc_WSAGSExtension, httpPostData, layerDefs);
                        bytes_WMSResponse = Convert.FromBase64String(respAGSExtension.WMSdata);
                        responseContentType = respAGSExtension.mimeType;
                        String respuestaStr = System.Text.Encoding.UTF8.GetString(bytes_WMSResponse);

                        int indiceFinCabecera = respuestaStr.IndexOf("<sld:NamedLayer>");
                        string cabeceraXML = respuestaStr.Substring(0, indiceFinCabecera);
                        if (cabeceraXML.Contains("<!--"))
                        {
                            indiceFinCabecera = respuestaStr.IndexOf("<!--");
                            cabeceraXML = respuestaStr.Substring(0, indiceFinCabecera);
                        }
                        string etiquetaFinal = "</sld:StyledLayerDescriptor>";

                        // 20180111 @ADR: Se habilita la devolución de una respuesta compuesta para mejorar el cumplimiento de las especificaciones WMS y SLD
                        // en lo que respecta al tratamiento de las agrupaciones ficticias (#3304).

                        if (hayRespuestaCompuesta)
                        {
                            string[] namedLayersRespuesta = new string[capasMXD.Length];
                            string contenido = "";
                            for (int i = 0; i < capasMXD.Length; i++)
                            {
                                string SLDrespuesta = capasMXD[i].Contains(",") ? SLDstrGrupos : respuestaStr;
                                string[] capasSLD = capasMXD[i].Contains(",") ? arrLayersReq : capasMXD;
                                int indiceInic = SLDrespuesta.IndexOf("<sld:Name>" + capasSLD[i] + "</sld:Name>");
                                int indiceFin = SLDrespuesta.IndexOf("</sld:NamedLayer>", indiceInic);
                                int longitudSeleccion = indiceFin - indiceInic;
                                contenido = SLDrespuesta.Substring(indiceInic, longitudSeleccion);
                                namedLayersRespuesta[i] = "<sld:NamedLayer>\r\n" + contenido + "</sld:NamedLayer>\r\n";
                            }
                        
                            string respuestaCompuesta = cabeceraXML + string.Join("", namedLayersRespuesta) + etiquetaFinal;
                            Response.Clear();
                            Response.ContentType = responseContentType;
                            string nombreFicheroSalida = "attachment; filename=SLD.xml";
                            Response.AddHeader("content-disposition", nombreFicheroSalida);
                            Response.Write(respuestaCompuesta);
                        }
                        else
                        {
                            int indiceLabelName = 0;
                            string todosEstilosEncontrados = "<sld:Name>" + layersReq + "</sld:Name>";
                            indiceLabelName = SLDstrEstilos.IndexOf("<sld:Name>" + layersReq + "</sld:Name>", indiceLabelName);

                            while (indiceLabelName != -1)
                            {
                                int indiceLabelUserStyle = SLDstrEstilos.IndexOf("<sld:UserStyle>", indiceLabelName);
                                int indiceLabelNamedLayer = SLDstrEstilos.IndexOf("</sld:NamedLayer>", indiceLabelName);
                                int longitudSeleccion = indiceLabelNamedLayer - indiceLabelUserStyle;
                                todosEstilosEncontrados = todosEstilosEncontrados + SLDstrEstilos.Substring(indiceLabelUserStyle, longitudSeleccion);
                                indiceLabelName = SLDstrEstilos.IndexOf("<sld:Name>" + layersReq + "</sld:Name>", indiceLabelName + 1);
                            }

                            string layerResultadoFinal = "<sld:NamedLayer>\r\n" + todosEstilosEncontrados + "</sld:NamedLayer>\r\n";
                            string ficheroCompleto = cabeceraXML + layerResultadoFinal + etiquetaFinal;

                            Response.Clear();
                            Response.ContentType = responseContentType;
                            string nombreFicheroSalida = "attachment; filename=Styles" + layersReq + ".xml";
                            Response.AddHeader("content-disposition", nombreFicheroSalida);
                            Response.Write(ficheroCompleto);
                        } 
                    }
                    break;
 
                case "GETLEGENDGRAPHIC":
                    
                    if (Request.HttpMethod == "GET")
                    {
                        for (int i = 0; i < capasMXDConfig.Length; i++)
                        {
                            if (capasMXDConfig[i].Contains(layerReq) && capasMXDConfig[i].Contains(","))
                            {
                                strQueryString = HttpUtility.UrlEncode(strQueryString, System.Text.Encoding.UTF8).Replace("+", "%20");
                                break;
                            }
                            else
                            {
                                capasMXD = ObtenerCapasMXD(arrLayerReq, capasMXDConfig, capasConfig);
                                for (int k = 0; k < capasMXD.Length; k++)
                                {
                                    string[] capasMXDsplit = capasMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                                    RequestQueryString.Set("LAYER", capasMXDsplit[0]);
                                    strQueryString = reconstruirStrQueryString(RequestQueryString);
                                }
                                strQueryString = HttpUtility.UrlEncode(strQueryString, System.Text.Encoding.UTF8).Replace("+", "%20");
                                break;
                            }
                        }
                        httpPostData = wsArcGISServiceExtension.GetToPost(strQueryString);
                    }
                    respAGSExtension = wsArcGISServiceExtension.get_Data(LayerDesc_WSAGSExtension, httpPostData, layerDefs);
                    bytes_WMSResponse = Convert.FromBase64String(respAGSExtension.WMSdata);
                    responseContentType = respAGSExtension.mimeType;
                    
                    Response.Clear();
                    Response.ContentType = responseContentType;
                    Response.BinaryWrite(bytes_WMSResponse);
                    break;

                case "CAPABILITIES":
                case "GETCAPABILITIES":

                    // Obtenemos la versión
                    string strVersion;
                    trazaGeneral = "Es virtual. Obtenemos la version";
                    if (RequestQueryString["VERSION"] != null)
                    {
                        strVersion = RequestQueryString["VERSION"];
                        trazaGeneral = "Version =" + strVersion;
                    }
                    else
                    {
                        strVersion = "1.3.0";
                    }

                    System.Xml.XmlDocument m_xmld;
                    string pathFichero, pathBase;
                    string nombreFichero;
                    nombreFichero = strVersion.Replace(".", "");
                    trazaGeneral = "Nombre Fichero=" + nombreFichero;
                    m_xmld = new System.Xml.XmlDocument();

                    pathBase = this.Page.MapPath(this.AppRelativeTemplateSourceDirectory);
                    try
                    {
                        pathFichero = System.Configuration.ConfigurationManager.AppSettings.Get("PathCapabilities");
                        trazaGeneral = "Intentamos cargar el fichero " + pathBase + pathFichero + nombreFichero + ".xml";
                        System.IO.StreamReader sr = new System.IO.StreamReader(pathBase + pathFichero + nombreFichero + ".xml");
                        string strCapabilities = sr.ReadToEnd();
                        sr.Close();
                        trazaGeneral = "Cargado pero no leido";
                        trazaGeneral = "Lo hemos cargado";
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(strCapabilities);
                    }
                    catch (Exception ex)
                    {

                        string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + "    " + trazaGeneral + " ]]></ServiceException></ServiceExceptionReport>";
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(strError);
                    }

                    break;
                
                case "GETMAP":
                case "MAP":

					wsArcGISServiceExtension.Timeout = 10000000;

                    ImageHandler img = new ImageHandler();
                    if (Request.HttpMethod == "GET")
                    {
                        strQueryString = HttpUtility.UrlEncode(strQueryString, Encoding.GetEncoding("utf-8")).Replace("+","%20");
                        httpPostData = wsArcGISServiceExtension.GetToPost(strQueryString);
                    }
                    //Tenemos que obtener las coordenadas para calcular la escala
                    //
                    if (System.Configuration.ConfigurationManager.AppSettings.Get("EscalaMinima") != "")
                    {
                        string bbox = RequestQueryString["BBOX"];
                        string[] coords = bbox.Split(",".ToCharArray());
                        string SRS = "";

                        if (RequestQueryString["CRS"] != null)
                            SRS = RequestQueryString["CRS"];
                        else
                            SRS = RequestQueryString["SRS"];

                        if (RequestQueryString["VERSION"] != null)
                        {
                            strVersion = RequestQueryString["VERSION"];
                        }
                        else
                        {
                            strVersion = "1.3.0";
                        }
                        //Mínimas
                        double minX, minY, maxX, maxY;
                        string strDecimales = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                        string strMiles = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
                        if (//strVersion == "1.3.0" && 
                            (
                            SRS == "EPSG:4326" || SRS == "EPSG:4230" ||
                            SRS == "EPSG:3034" || SRS == "EPSG:3035" ||
                            SRS == "EPSG:3395" || SRS == "EPSG:4258" ||
                            SRS == "EPSG:4267" || SRS == "EPSG:4269" ||
                            SRS == "EPSG:4324"
                            )
                        )
                        {
                            minX = double.Parse(coords[1].Replace(strMiles, strDecimales));
                            minY = double.Parse(coords[0].Replace(strMiles, strDecimales));
                            maxX = double.Parse(coords[3].Replace(strMiles, strDecimales));
                            maxY = double.Parse(coords[2].Replace(strMiles, strDecimales));
                        }
                        else
                        {
                            minY = double.Parse(coords[1].Replace(strMiles, strDecimales));
                            minX = double.Parse(coords[0].Replace(strMiles, strDecimales));
                            maxY = double.Parse(coords[3].Replace(strMiles, strDecimales));
                            maxX = double.Parse(coords[2].Replace(strMiles, strDecimales));
                        }
                        int width = int.Parse(RequestQueryString["WIDTH"]);
                        int height = int.Parse(RequestQueryString["HEIGHT"]);
                        
                        double escala = calcularEscala(SRS, minX, minY, maxX, maxY, width, height);
                        double escalaMinima = double.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("EscalaMinima"));
                        if (escala > escalaMinima)
                        {
                            responseContentType = "image/png";
                            bytes_WMSResponse = img.CrearImagenBlanco((int)width, (int)height);
                            bytes_WMSResponse = img.WatermarkImage(bytes_WMSResponse, responseContentType);
                        }
                        else
                        {
                            respAGSExtension = wsArcGISServiceExtension.get_Data(LayerDesc_WSAGSExtension, httpPostData, layerDefs);
                            bytes_WMSResponse = Convert.FromBase64String(respAGSExtension.WMSdata);
                            responseContentType = respAGSExtension.mimeType;
                        }
                            
                    }
                    else
                    {

                        respAGSExtension = wsArcGISServiceExtension.get_Data(LayerDesc_WSAGSExtension, httpPostData, layerDefs);
                        bytes_WMSResponse = Convert.FromBase64String(respAGSExtension.WMSdata);
                        responseContentType = respAGSExtension.mimeType;
                    }
                    if (responseContentType == "application/vnd.ogc.se_xml")
                    {
                        //Se ha producido algún tipo de error...
                        string respStr = System.Text.Encoding.Default.GetString(bytes_WMSResponse);
                        Response.Clear();
                        Response.ContentType = responseContentType;
                        Response.BinaryWrite(bytes_WMSResponse);
                        break;
                    }
                    string pathImagen = System.Configuration.ConfigurationManager.AppSettings.Get("pathWatermark");
                    string respStr1 = System.Text.Encoding.Default.GetString(bytes_WMSResponse);


                    string tipoMarca = System.Configuration.ConfigurationManager.AppSettings.Get("watermark");
                    byte[] salida;
                    if (tipoMarca == "texto")
                        salida = img.WatermarkImage(bytes_WMSResponse, responseContentType);
                    else
                        salida = img.WatermarkImage2(bytes_WMSResponse, responseContentType, pathImagen);

                    Response.Clear();
                    Response.ContentType = responseContentType;
                    Response.BinaryWrite(salida);

                    break;

                //Para GetFeatureInfo debemos realizar el cambio de hoja de estilo
                case "FEATUREINFO":
                case "GETFEATUREINFO":
                    
                    bool esCapaIdentificablePorEscala = false;
                    bool esGrupoIdentificable = false;
                    string[] capasMXDInd = null;
                    string[] titulosMXDInd = null;

                    capasMXD = ObtenerCapasMXD(arrQueryLayersReq, capasMXDConfig, capasConfig);
                    capasIdentificacion = ObtenerCapasIdentificacion(arrQueryLayersReq, capasIdentificacionConfig, capasConfig);
                    titulosMXD = ObtenerTitulosMXD(arrQueryLayersReq, titulosMXDConfig, capasConfig, capasMXDConfig);
                    // 20180111 @ADR: Para obtener capasPeticiones se eliminan las capas no identificables,
                    // por lo que se elimina el control que anteriormente se realizaba a través de bool esCapaIdentificable
                    capasPeticiones = ObtenerCapasPeticion(capasMXD, capasIdentificacion, titulosMXD);

                    for (int i = 0; i < capasPeticiones.Length; i++)
                    {
                        if (capasPeticiones[i].Contains(","))
                        {
                            capasMXDInd = capasMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                            titulosMXDInd = titulosMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                            esGrupoIdentificable = capasIdentificacion[i].IndexOf("1") != -1;
                            break;
                        }
                    }

                    if (esGrupoIdentificable)
                    {
                        // Para mapas con varias capas en los que únicamente será visible una sola capa a una escala determinada
                        //1) Obtenemos la escala mínima de cada Layer
                        System.Xml.XmlNodeList nodosLayer = getLayersRest(ServidorArcGIS, ServicioArcGIS);
                        int layerCount = nodosLayer[0].ChildNodes.Count;
                        double[] arrayEscalasLayersArcGIS = new double[layerCount];
                        string[] nombresLayersArcGIS = new string[layerCount];
                        for (int k = 0; k < layerCount; k++)
                        {
                            if (nodosLayer[0].ChildNodes[k].SelectSingleNode("./minScale").InnerText == "0")// ? "True" : "False";
                            {
                                arrayEscalasLayersArcGIS[k] = escalaMin;
                            }
                            else
                            {
                                arrayEscalasLayersArcGIS[k] = double.Parse(nodosLayer[0].ChildNodes[k].SelectSingleNode("./minScale").InnerText); // guardamos las escalas mínimas de cada ILayer
                            }
                            nombresLayersArcGIS[k] = nodosLayer[0].ChildNodes[k].SelectSingleNode("./name").InnerText;
                        }

                        //2) Obtenemos la escala a la que está el mapa en la petición de identificación
                        // Para ello, necesitamos obtener de la petición:
                        // Sistema de Referencia
                        // BBOX: xmin, xmax, ymin, ymax
                        // Ancho y alto
                        // e invocamos al método calcularEscala con esos parámetros

                        double p_minX, p_minY, p_maxX, p_maxY;
                        bool p_ancho = true;
                        int width = int.Parse(Request.QueryString["WIDTH"]);
                        int height = int.Parse(Request.QueryString["HEIGHT"]);
                        if (height > width)
                            p_ancho = false;

                        string strDecimales = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                        string strMiles = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;

                        string bbox = Request.QueryString["BBOX"];
                        string[] bb = bbox.Split(",".ToCharArray());
                        p_minX = double.Parse(bb[0].Replace(strMiles, strDecimales));
                        p_minY = double.Parse(bb[1].Replace(strMiles, strDecimales));
                        p_maxX = double.Parse(bb[2].Replace(strMiles, strDecimales));
                        p_maxY = double.Parse(bb[3].Replace(strMiles, strDecimales));

                        string p_SRS = "";
                        bool esCRSGeograficas = false;
                        double escalaMapa = 0;
                        if (Request.QueryString["CRS"] != null)
                            p_SRS = Request.QueryString["CRS"];
                        else
                            p_SRS = Request.QueryString["SRS"];

                        if (
                            p_SRS == "EPSG:4326" || p_SRS == "EPSG:4230" ||
                            p_SRS == "EPSG:3034" || p_SRS == "EPSG:3035" ||
                            p_SRS == "EPSG:3395" || p_SRS == "EPSG:4258" ||
                            p_SRS == "EPSG:4267" || p_SRS == "EPSG:4269" ||
                            p_SRS == "EPSG:4324"
                            )
                        {
                            esCRSGeograficas = true;
                        }

                        if (esCRSGeograficas)
                            escalaMapa = calcularEscala(p_SRS, p_minY, p_minX, p_maxY, p_maxX, width, height); // lat, long
                        else
                            escalaMapa = calcularEscala(p_SRS, p_minX, p_minY, p_maxX, p_maxY, width, height); // lon, lat

                        //3) En función de la escala obtenida y los valores de escala mínima de cada capa, podremos saber qué capa
                        // se muestra para poder determinar si es identificable o no.

                        for (int i = 0; i < capasPeticiones.Length; i++)
                        {
                            for (int k = 0; k < layerCount; k++)
                            {
                                if (capasPeticiones[i].Contains(","))
                                {
                                    for (int j = 0; j < capasMXDInd.Length; j++)
                                    {
                                        if (escalaMapa <= arrayEscalasLayersArcGIS[k] && nombresLayersArcGIS[k] == capasMXDInd[j])
                                        {
                                            esCapaIdentificablePorEscala = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (escalaMapa <= arrayEscalasLayersArcGIS[k] && nombresLayersArcGIS[k] == capasMXD[i])
                                    {
                                        esCapaIdentificablePorEscala = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        esCapaIdentificablePorEscala = true;
                    }

                    peticiones = new string[capasPeticiones.Length];

                    for (int j = 0; j < peticiones.Length; j++)
                    {
                        peticiones[j] = strQueryString.Replace("QUERY_LAYERS=" + queryLayersReq, "QUERY_LAYERS=" + capasPeticiones[j]);
                    }

                    if (cambio_FormatInfo_FeatureInfo)
                    {
                        string[] strValores;
                        string inicio = "<FeatureInfoResponse>";
                        string final = "</FeatureInfoResponse>";
                        strValores = new string[peticiones.Length];
                        bool[] hayResultados = new bool[peticiones.Length];

                        for (int i = 0; i < peticiones.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(peticiones[i])) // Si peticiones es null es que no es identificable
                            {
                                if (!esCapaIdentificablePorEscala)
                                {
                                    Response.Redirect("NoQueryEscala.htm");
                                    break;
                                }

                                peticiones[i] = HttpUtility.UrlEncode(peticiones[i], System.Text.Encoding.UTF8).Replace("+", "%20"); // arg
                                httpPostData = wsArcGISServiceExtension.GetToPost(peticiones[i]);
                                respAGSExtension = wsArcGISServiceExtension.get_Data(LayerDesc_WSAGSExtension, httpPostData, layerDefs);
                                respuesta = Convert.FromBase64String(respAGSExtension.WMSdata);

                                string respStr2 = System.Text.Encoding.GetEncoding("UTF-8").GetString(respuesta);
                                //parseamos a xml
                                System.Xml.XmlDocument xm = new System.Xml.XmlDocument();
                                xm.LoadXml(respStr2);
                                System.Xml.XmlNodeList nNombre = xm.GetElementsByTagName("SERVICEEXCEPTIONREPORT");
                                //Controlamos que no hay un error en la respuesta

                                if (nNombre.Count == 0)
                                {
                                    string nombreLayer = "";
                                    string nombreGroup = "";

                                    if (capasPeticiones[i].Contains(","))
                                    {
                                        string[] groupName = ObtenerTituloGrupo(arrQueryLayersReq, titulosConfig, capasConfig);
                                        nombreGroup = groupName[i];
                                        for (int r = 0; r < titulosMXDInd.Length; r++)
                                        {
                                            //if (titulosMXDInd[r] == capasMXDInd[r])
                                            //{
                                                nombreLayer = titulosMXDInd[r];
                                                break;
                                            //}
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < capasConfig.Length; k++)
                                        {
                                            // 20180118 @ADR: Fix #3541
                                            if (capasPeticiones[i] == capasConfig[k] || capasPeticiones[i] == "0" || arrQueryLayersReq[i] == capasConfig[k])
                                            {
                                                nombreLayer = titulosConfig[k];
                                                break;
                                            }
                                        }
                                    }

                                    System.Xml.XmlNodeList campoFeatureInfo = xm.GetElementsByTagName("FeatureInfo");
                                    hayResultados[i] = campoFeatureInfo.Count == 0 ? false : true;
                                    if (!hayResultados[i])
                                    {
                                        break;
                                    }
                                    System.Xml.XmlNodeList featureInfoResp = xm.GetElementsByTagName("FeatureInfoResponse");

                                    if (featureInfoResp.Count != 0)
                                    {
                                        strValores[i] = "";

                                        for (int j = 0; j < featureInfoResp[0].ChildNodes.Count; j++)
                                        {

                                            System.Xml.XmlNode FeatureInfoCol = featureInfoResp[0].ChildNodes[j];

                                            if (capasPeticiones[i].Contains(","))
                                            {
                                                for (int r = 0; r < titulosMXDInd.Length; r++)
                                                {
                                                    //if (FeatureInfoCol.Attributes["layername"].Value == titulosMXDInd[r])
                                                    //{
                                                        nombreLayer = titulosMXDInd[r];
                                                        break;
                                                    //}
                                                }
                                            }
                                            else
                                            {
                                                for (int k = 0; k < capasConfig.Length; k++)
                                                {
                                                    // 20180118 @ADR: Fix #3541
                                                    if (FeatureInfoCol.Attributes["layername"].Value == capasConfig[k] || FeatureInfoCol.Attributes["layername"].Value == "0" || arrQueryLayersReq[i] == capasConfig[k])
                                                    {
                                                        nombreLayer = titulosConfig[k];
                                                        break;
                                                    }
                                                }
                                            }

                                            System.Xml.XmlNodeList FeatureInfo = FeatureInfoCol.ChildNodes;
                                            if (FeatureInfo.Count < 1)
                                            {
                                                continue;
                                            }
                                            strValores[i] += "<LAYER NAME=\"" + nombreLayer + "\" GROUP=\"" + nombreGroup + "\">";
                                            System.Xml.XmlNodeList Fields = FeatureInfo[0].ChildNodes;

                                            for (int ii = 0; ii < FeatureInfo.Count; ii++)
                                            {
                                                strValores[i] += "<FIELDS";


                                                foreach (System.Xml.XmlElement nodo in FeatureInfo[ii].ChildNodes)
                                                {
                                                    System.Xml.XmlNodeList Childs = nodo.ChildNodes;
                                                    string fName = nodo.ChildNodes[0].InnerText.Replace(" ", "");
                                                    if (fName.Equals("OBJECTID") == false)
                                                    {
                                                        fName = fName.Replace("(", "");
                                                        fName = fName.Replace(")", "");
                                                        fName = fName.Replace(":", "");
                                                        fName = fName.Replace("º", "");
                                                        fName = fName.Replace("/", "");
                                                        fName = fName.Replace("?", "");
                                                        fName = fName.Replace("¿", "");
                                                        fName = fName.Replace("€", "");
                                                        fName = fName.Replace("<", "");
                                                        fName = fName.Replace(">", "");
                                                        // 20170828 @ALX: se reemplaza también el % ya que no
                                                        // se admite us uso como nombre de atributo de un nodo XML.
                                                        fName = fName.Replace("%", "");
                                                        // 20170828 @ALX: se quitan los números al principio del atributo porque
                                                        // también fallan.
                                                        var pattern = @"^\d*";
                                                        var regex = new Regex(pattern);
                                                        fName = regex.Replace(fName, "");
                                                        string fValue = nodo.ChildNodes[1].InnerText;//.Replace(" ", "");
                                                        // 20170914 @ADR: Añadido control de flujo para detectar si el valor del atributo es un hipervínculo
                                                        // y tratar el caracter & de manera diferenciada.
                                                        // https://stackoverflow.com/questions/5709232/how-do-i-include-etc-in-xml-attribute-values
                                                        bool esurl = fValue.ToUpper().IndexOf("HTTP://") == 0 || fValue.ToUpper().IndexOf("HTTPS://") == 0;
                                                        if (!esurl)
                                                        {
                                                            fValue = fValue.Replace("Null", "-");
                                                            fValue = fValue.Replace("\"", "'");
                                                            fValue = fValue.Replace("NULL", "-");
                                                            fValue = fValue.Replace("null", "-");
                                                            fValue = fValue.Replace("&", " y ");
                                                            fValue = fValue.Replace("<", "&lt;");
                                                            // 20180129 @ADR: Se añade la siguiente sustitución para campos que contienen comillas
                                                            fValue = fValue.Replace(" y quot;", "'");
                                                            //fValue = fValue.Replace("*", "");
                                                        }
                                                        else
                                                        {
                                                            fValue = fValue.Replace("&", "&amp;");
                                                        }
                                                        strValores[i] = strValores[i] + " " + fName + "=\"" + fValue + "\"";
                                                    }
                                                }
                                                strValores[i] = strValores[i] + "></FIELDS>";
                                            }
                                            strValores[i] += "</LAYER>";
                                        }
                                    }
                                }
                                else
                                {
                                    Response.Clear();
                                    Response.ContentType = "text/xml";
                                    Response.Write(respStr2);
                                    return;
                                }
                            }
                            else
                            {
                                // Es identificable o no se han encontrado valores
                                Response.Redirect("NoQuery.htm");
                                break;
                            }
                        }

                        if (System.Array.IndexOf(hayResultados, true) < 0)
                        {
                            Response.Redirect("NoData.htm", false);
                        }

                        string strRespuesta;
                        strRespuesta = inicio;
                        for (int j = 0; j < strValores.Length; j++)
                        {
                            strRespuesta = strRespuesta + strValores[j];
                        }
                        strRespuesta = strRespuesta + final;
                        byte[] buffer = Encoding.GetEncoding("UTF-8").GetBytes(strRespuesta);
                        System.IO.MemoryStream input = new System.IO.MemoryStream(buffer);
                        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(input);
                        System.Xml.Xsl.XslCompiledTransform myXslTransform;

                        myXslTransform = new System.Xml.Xsl.XslCompiledTransform();
                        myXslTransform.Load(myPath + "/estilos/wms_featureinfo_html.xsl");
                        System.IO.MemoryStream output = new System.IO.MemoryStream();
                        System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(output, Encoding.GetEncoding("UTF-8"));
                        myXslTransform.Transform(reader, writer);
                        Response.Clear();
                        Response.ContentType = "text/html";
                        Response.Write(Encoding.GetEncoding("UTF-8").GetString(output.ToArray()));
                    }
                    else // Si no hay un cambio de formato (text/plain)
                    {
                        if (!esCapaIdentificablePorEscala)
                        {
                            respuesta = null; // No devuelve nada
                        }
                        else
                        {
                            if (peticiones != null)
                            {
                                // TODO Habría que tratar esto para peticiones múltiples
                                strQueryString = peticiones[0]; // arg limitado a una consulta
                            }

                            if (Request.HttpMethod == "GET")
                            {
                                strQueryString = HttpUtility.UrlEncode(strQueryString, Encoding.GetEncoding("utf-8")).Replace("+", "%20"); // arg

                                httpPostData = wsArcGISServiceExtension.GetToPost(strQueryString);
                            }
                            respAGSExtension = wsArcGISServiceExtension.get_Data(LayerDesc_WSAGSExtension, httpPostData, layerDefs);
                            respuesta = Convert.FromBase64String(respAGSExtension.WMSdata);
                        }
                        if (respuesta != null)
                        {
                            Response.Clear();
                            Response.ContentType = responseContentType;
                            Response.BinaryWrite(respuesta);
                        }
                    }

                    break;
                default:
                    //Generamos el mensaje de error y lo mandamos por pantalla
                    string strError2 = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ Petición REQUEST no soportada.  ]]></ServiceException></ServiceExceptionReport>";
                    Response.Clear();
                    Response.ContentType = "text/xml";
                    Response.Write(strError2);
                    break;

            }
        }    
        catch (Exception ex)
        {
            string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + "    " + trazaGeneral + " "+ ex.ToString() + " ]]></ServiceException></ServiceExceptionReport>";
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(strError);
        }
        finally
        {
        }
    }
}